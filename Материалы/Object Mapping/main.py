import psycopg2
from psycopg2.extras import RealDictCursor

conn = psycopg2.connect("""
password=postgres dbname=postgres user=postgres port=38746 host=localhost
""", cursor_factory=RealDictCursor)


class StorageCell:
    def __init__(self, id: int, code: str, capacity: int):
        self.id = id
        self.code = code
        self.capacity = capacity

    def get_info(self):
        return f"""Код: {self.code}, объём: {self.capacity}, id: {self.id}"""


class Equipment:
    def __init__(self, id: int, title: str, color: str):
        self.id = id
        self.title = title
        self.color = color

    def get_info(self):
        return f"""Название: {self.title}, цвет: {self.color}, id: {self.id}"""


class Holder:
    def __init__(self, id: int, name: str, phone: str):
        self.id = id
        self.name = name
        self.phone = phone
        self.storage_cells = []
        self.equipment = []

    def show_info(self):
        print(f"""Имя: {self.name}, телефон: {self.phone}, id: {self.id}""")

        if len(self.equipment):
            print("    экипировка:")
            for sc in self.equipment:
                print("      " + sc.get_info())

        if len(self.storage_cells):
            print("    ячейки хранения:")
            for sc in self.storage_cells:
                print("      " + sc.get_info())


def print_holders():
    cur = conn.cursor()

    cur.execute("""
select h.id    holder_id,
       h.name,
       h.phone,
       c.id    cell_id,
       c.code,
       c.capacity,
       e.id as equipment_id,
       title,
       color
from holder h
         left join equipment_to_holder eth on h.id = eth.holder_id
         left join equipment e on eth.equipment_id = e.id
         left join storage_cell c on h.id = c.holder_id;
    """)
    rows = cur.fetchall()

    holders_dict = {}
    cells_dict = {}
    equipment_dict = {}
    for row in rows:
        holder_id = row['holder_id']
        if holder_id not in holders_dict:
            holder = Holder(row['holder_id'], row['name'], row['phone'])
            holders_dict[holder_id] = holder
        else:
            holder = holders_dict[holder_id]

        cell_id = row['cell_id']
        if cell_id is not None and cell_id not in cells_dict:
            storage_cell = StorageCell(row['cell_id'], row['code'], row['capacity'])
            cells_dict[cell_id] = storage_cell
        elif cell_id is not None:
            storage_cell = cells_dict[cell_id]

        if cell_id is not None and storage_cell not in holder.storage_cells:
            holder.storage_cells.append(storage_cell)

        equipment_id = row['equipment_id']
        if equipment_id is not None and equipment_id not in equipment_dict:
            equipment = Equipment(equipment_id, row['title'], row['color'])
            equipment_dict[equipment_id] = equipment
        elif equipment_id is not None:
            equipment = equipment_dict[equipment_id]

        if equipment_id is not None and equipment not in holder.equipment:
            holder.equipment.append(equipment)

    for holder in holders_dict.values():
        holder.show_info()


print_holders()
