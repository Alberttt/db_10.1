### 1. Создать таблицы и связи, заполнить таблицы данными.
### 2. Написать запрос на вывод сущности со всеми связанными сущностями, сделать преобразование из табличного представления в объектное [пример](https://gitlab.com/golodnyuk.iv/db_10.1/-/tree/main/%D0%9C%D0%B0%D1%82%D0%B5%D1%80%D0%B8%D0%B0%D0%BB%D1%8B/Object%20Mapping)

#### Требования:
- у каждой сущности должен быть [автогененрируемый первичный ключ](https://gitlab.com/golodnyuk.iv/db_10.1/-/blob/main/%D0%9C%D0%B0%D1%82%D0%B5%D1%80%D0%B8%D0%B0%D0%BB%D1%8B/07.pk_generation.md)
- наличие many-to-many связи между сущностями
- наличие либо one-to-many, либо one-to-one связи между сущностями
- у каждой сущности должно быть минимум 3 свойства, всего минимум 12 свойств (кроме первичных и внешних ключей)

| ФИО | Тема                                                                                                                                                                                                  |
|-----|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Суранов | АТС |
| Величко | Агрегаторы и таксисты: в агрегаторе может работать много таксистов, каждый таксист может работать в нескольких аггрегаторах. У таксиста может быть много заказов.                                     |
| Веренёв | Статьи и пользователи: у статьи может быть много просмотров от разных пользователей, у пользователя может быть много просмотренных статей. У статьи может быть много комментариев.                    |
| Будникова | Задачник: пользователь может решить много задач, каждую задачу может решить много пользователей. Задачи могут быть объединены в группы (например по тематике).                                        |
| Омелькова | Поставщики и продукты: поставщик может поставлять множество продуктов, разные поставщики могут поставлять один и тот же продукт. У поставщика может быть много транспортных средств.                  |
| Пыхтин | Игровой магазин: у пользователя может быть куплено много игр, каждую игру может купить много пользователей. У игры может быть много отзывов.                                                          |
| Песковацков | Агрегатор сервисов аренды автомобилей: каждый сервис может сдавать много различных моделей автомобилей, у разных сервисов может встречаться одни и те же модели. У сервиса может быть много филиалов. |
| Рябинина | Фильмы и актёры: актёр может сниматься во многих фильмах, в фильме может сниматься много актёров. У актёра может быть много наград.                                                                   |
| Субботина | Музыка и исполнители: у исполнителя может быть много музыкальных произведений, авторами музыкальных произведений могут быть несколько исполнителей. У исполнителя может быть много гастролей.         |
| Ларин | Книги и авторы: у автора может быть много книг, у книги может быть несколько авторов. У книги может быть несколько переводов.                                                                         |
| Суслова | Картины и художники: у художника может быть много картин, у картины может быть несколько авторов. У картины может быть много рецензий.                                                                |
|     | Трэкер заданий: у исполнителя может быть много задач, над задачей может работать несколько исполнителей. В задаче может быть много комментариев.                                                      |
| Махрова | Репозитории и юзеры: у юзера может быть несколько репозиториев, в репозиторий могут контрибьютить много юзеров. В репозитории может быть много тикетов.                                               |
| Щанников | Видеохостинг: видео могут просмотреть много юзеров, юзер может просмотреть много видео. У видео может быть много комментариев.                                                                        |
| Орлова | Мероприятия и участники: в мероприятии может участвовать много человек, человек может участвовать во многих мероприятиях. В мероприятии может быть несколько частей (программа).                      |
| Маргачёва | Доставка и курьеры: у доставки может быть много курьеров, курьер может обслуживать много доставок. У курьера может быть много заказов.                                                                |
| Латыпова | Врачи и пациенты: пациент может лечиться у многих врачей, у врача может быть много пациентов. У пациента есть история посещения врачей с результатами                                                 |
| Ильин | Компании и работники: в компании может работать много работников, работник может работать в нескольких компаниях. У работника есть много личных документов.                                           |
| Сарибекян    | gist клон: юзер может отметить много заметок, у заметки может быть много отметок от разных пользователей. У заметки может быть много комментариев.                                                    |
| Корелин | научные статьи и рецензии: у статьи может быть много авторов, у автора может быть много статей. У статьи может быть несколько рецензий.                                                               |
