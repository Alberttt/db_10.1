drop table if exists games, users, review, games_to_users cascade;

create table users
(
	id int generated always as identity primary key,
	name text,
	registration_date timestamptz,
	profile_level int
);


create table games
(
	id int generated always as identity primary key,
	name text,
	genre text,
	age_limit int
);


create table review
(
	id int generated always as identity primary key,
	game_id int references games,
	user_id_review int references users,
	review_text text,
	score int,
	time_of_writing timestamptz
);


create table games_to_users
(
	user_id int references users,
	game_id int references games,
	primary key (user_id, game_id)
);


insert into users(name, registration_date, profile_level)
values ('Alpha', '2017-02-02T17:23:44+0300', 68),
('Beta', '2022-12-01T00:40:24+0300', 15),
('Gamma', '2019-06-12T22:15:04+0300', 3),
('Delta', '2009-01-31T16:34:22+0300', 184),
('Sigma', '2014-09-01T18:48:45+0300', 44);

insert into games (name, genre, age_limit)
values ('Counter Strike', 'FPS', 16),
('Valorant', 'FPS', 12),
('Cyberpunk 2077', 'science fiction, action, FPS', 18),
('Tomb Raider', 'Adventure', 18),
('Minecraft','Open World', 12);


insert into games_to_users(game_id, user_id)
values (1, 1),
(1, 3),
(2, 4),
(2, 5),
(3, 1),
(3, 4),
(4, 2),
(4, 5),
(5, 3),
(5, 4);


insert into review (game_id, user_id_review, review_text, score, time_of_writing)
values (1, 1, 'Хорошо', 7, '2017-02-02T17:23:44+0300'),
(1, 3, 'Плохо', 2, '2017-02-02T17:23:44+0300'),
(2, 5, 'Отлично', 8, '2022-12-01T00:40:24+0300'),
(2, 4, 'Не очень', 4, '2022-12-01T00:40:24+0300'),
(3, 1, 'Нормально', 5, '2021-06-12T22:15:04+0300'),
(3, 5, 'Супер', 8, '2021-06-12T22:15:04+0300'),
(4, 2, 'Классно', 7, '2015-01-31T16:34:22+0300'),
(4, 5, 'Ужасно', 1, '2015-01-31T16:34:22+0300'),
(5, 4, 'Прекрасно', 8, '2009-01-31T16:34:22+0300'),
(5, 5, 'Шедеврально', 10, '2009-01-31T16:34:22+0300');


select u.name, registration_date, profile_level, g.name as game_name, genre, age_limit, user_id_review as user_id, review_text as review, score, time_of_writing
from games g
left join review r on g.id = r.game_id
join games_to_users gu on g.id = gu.game_id
join users u on u.id = gu.user_id;
