drop table if exists tour, city, rewiew, city_to_tour cascade;


create table tour
(
	id int primary key,
	name text,
	price numeric
);

create table rewiew
( 
  id int primary key,
 comment text,
 rating int
);
 

create table city
(
	name text primary key,
	founded int
);

create table city_to_tour

(
	city_name text references city,
	tour_id int references tour,
	primary key (city_name, tour_id)
);

insert into tour (id, name, price)
values (1, 'В гостях у хаски', 1000),
       (2, 'Хиты Карелии', 2000),
      (3, 'В гостях у хаски', 3000),
       (4, 'Хиты Карелии', 4000);
      
insert into city (name, founded)
values ('Приозерск', 1295),
       ('Сортавала', 1468),
       ('Валаам', 1407);
      
insert into rewiew (comment, rating, id)
values ('Отличный тур', 5, 1),
    ('Не понравилось', 2, 2),
    ('Хорошо', 4, 3),
    ('Нормально', 3, 4);
   
insert into city_to_tour (tour_id, city_name)
values (1, 'Приозерск'),
       (2, 'Сортавала'),
       (3, 'Сортавала'),
       (4, 'Валаам');
      
      
select t.name as tour_name, city_name as city, founded, price,rating, comment
from tour t
	left join city_to_tour ct on t.id =ct.tour_id
	left join city c on c.name = ct.city_name
	left join rewiew r on t.id=r.id;
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      