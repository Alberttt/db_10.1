drop table if exists tour cascade;
drop table if exists review cascade;


create table tour 
( 
	id int generated always as identity primary key,
	name text,
	price int
);


create table review
(
	id int generated always as identity primary key,
	comment text,
	rating int,
	tour_id int references tour
);

insert into tour (name, price)
values 	('С приветом по планетам', 5990),
		('В гостях у деда мороза', 2990),
		('Северная Россия', 4099);
	
	
insert  into review (comment, rating, tour_id) 
values 	('Хорошо', 4, 1),
		('Круто', 5, 1),
		('Нормаьно', 3, 2),
		('Пойдёт', 3, 2),
		('Ужасно', 1, 3), 
		('На любителя', 4, 3);

		
select tour.id, tour.name, tour.price, review.comment, review.rating
from review
join tour on tour.id = review.tour_id;
