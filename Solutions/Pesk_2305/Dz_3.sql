
drop table if exists tour, city, review, tour_to_city cascade;

create table tour
(
	name text primary key,
	price numeric
);

create table city
(
	name text primary key ,
	founded int
);

create table review
(
	tour_name text references tour,
	comment text,
	rating int
);

create table tour_to_city
(
    tour_name text references tour,
    city_name text references city,
    primary key (tour_name, city_name)
);

insert into tour (name,price)
values ('В гостях у хаски',1300),
	   ('Хиты карелии',15000);

insert into city (name,founded)
values ('Приозерск',2000),
	   ('Сортавала',1000),
	   ('Валаам',500);

insert into review (tour_name,comment,rating)
values ('В гостях у хаски','Отличный тур!',10),
	   ('Хиты карелии','Не понравилось.',1);
	  
insert into tour_to_city (tour_name,city_name)
values ('В гостях у хаски','Приозерск'),
	   ('В гостях у хаски','Сортавала'),
	   ('Хиты карелии','Сортавала'),
	   ('Хиты карелии','Валаам');

select *
from tour t
         join tour_to_city tc on t.name = tc.tour_name
         join city c on c.name = tc.city_name
         join review r on t.name = r.tour_name;

