
DROP TABLE IF exists users, settings, CASCADE;
 
CREATE TABLE users
(
nickname text PRIMARY KEY,
first_name text,
last_name text
);
 
CREATE TABLE settings
(
font_size int,
color_scheme text,
user_nickname text references users unique
);
 
INSERT INTO users (nickname, first_name, last_name)
VALUES ('katikovka', 'Екатерина', 'Орлова'),
       ('Kexibo', 'Виталий', 'Суранов');
      
INSERT INTO settings (font_size, color_scheme, user_nickname)
VALUES (14, 'Синий', 'katikovka'),
       (14, 'Жёлтый', 'Kexibo');
      
select *
from users
         join settings ON nickname = user_nickname;
