drop table if exists tour, city, review, city_to_tour cascade;        
create table tour
(
	price numeric,
	name text primary key
);

create table review
(
	id int,
	comment text,
	rating int,
	tour_name text references tour
);

create table city
(
    name text primary key,
    founded int
);

create table city_to_tour
(
    city_name text references city,
    tour_name text references tour,
    primary key (city_name, tour_name)
);

insert into tour
values (100, 'В гостях у хаски'),
(120, 'Хиты Карелии');
      
insert into review (id, comment, rating, tour_name)
values (1, 'Отличный тур', 5, 'Хиты Карелии'),
	   (2, 'Не понравилось', 2, 'В гостях у хаски');

insert into city (name, founded)
values ('Приозерск', 1295),
       ('Сортавала', 1468),
       ('Валаам', 1407);

insert into city_to_tour (tour_name, city_name)
values ('Хиты Карелии', 'Приозерск'),
       ('В гостях у хаски', 'Сортавала'),
       ('Хиты Карелии', 'Сортавала'),
       ('В гостях у хаски', 'Валаам');

select t.name, c.name, price, founded, rating, comment from tour t
left join review r on t.name = r.tour_name
left join city_to_tour ct on t.name = ct.tour_name
left join city c on c.name = ct.city_name;
