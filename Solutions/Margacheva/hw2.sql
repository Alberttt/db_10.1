drop table if exists settings, users cascade;


create table users
(
	id 		   int primary key,
    nickname   text,
    first_name text,
    second_name text
);

create table settings
(
    font_size       int,
    color_scheme    text,
    users_id int references users unique
);



insert into users
values (1, 'Albert2003', 'Альберт Андреевич','Тенигин'),
       (2, 'IVAN3000', 'Иван Вячеславович', 'Голоднюк');


insert into settings (font_size, color_scheme, users_id)
values (12, 'black', 1),
       (20, 'red', 2);

select *
from users
         join settings on id = users_id;

select nickname, font_size, color_scheme
from users
         join settings on id = users_id;





