drop table if exists tour, city, review, city_to_tour cascade;

create table tour
(
	name text primary key,
	price numeric
);

create table city
(
	name text primary key ,
	founded int
);

create table review
(
	tour_name text references tour,
	comment text,
	rating int
);

create table city_to_tour
(
    tour_name text references tour,
    city_name text references city,
    primary key (tour_name, city_name)
);

insert into tour (name,price)
values ('В гостях у хаски',2500),
	   ('Хиты карелии',5000);

insert into city (name,founded)
values ('Приозерск',1295),
	   ('Сортавала',1468),
	   ('Валаам',1407);

insert into review (tour_name,comment,rating)
values ('В гостях у хаски','Отличный тур!',10),
	   ('Хиты карелии','Не понравилось.',1);
	  
insert into city_to_tour (tour_name,city_name)
values ('В гостях у хаски','Приозерск'),
	   ('В гостях у хаски','Сортавала'),
	   ('Хиты карелии','Сортавала'),
	   ('Хиты карелии','Валаам');

select t.name, price, c.name, founded, comment, rating
from tour t
         join city_to_tour ct on t.name = ct.tour_name
         join city c on c.name = ct.city_name
         join review r on t.name = r.tour_name;

