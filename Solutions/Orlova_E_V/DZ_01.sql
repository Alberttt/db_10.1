-- удаление таблиц, если они уже созданы
DROP TABLE IF EXISTS tour, review CASCADE;
 
CREATE TABLE tour
(
    NAME  TEXT PRIMARY KEY, -- для задания ссылочной целостности задается первичный ключ (уникальный идентификатор строки) в главной таблице
    price NUMERIC
);
 
CREATE TABLE review
(
    id          INT,
    com         TEXT,
    rating      INT,
    tour_name   TEXT REFERENCES tour -- столбец, через который мы ссылаемся на главную таблицу
);
 
   
-- вставка данных
INSERT INTO tour (NAME, price)
VALUES ('Первый тур', 01),
       ('Второй тур', 02);
      
 
      
-- значения вставляемые в столбец holder_phone должны присутствовать в главной таблице в столбце phone
INSERT INTO review (id, com, rating, tour_name)
VALUES (0001, 'comment1', 1, 'Первый тур'),
       (0001, 'comment2', 1, 'Первый тур'),
       (0002, 'comment3', 2, 'Второй тур'),
       (0002, 'comment4', 2, 'Второй тур');
 
 
-- соединение таблиц по ключу и вывод всех колонок
SELECT *
FROM tour
         JOIN review ON tour_name = NAME;
        
-- соединение таблиц по ключу и вывод определённых колонок
SELECT price, NAME AS id, com, rating
FROM tour
         JOIN review ON tour_name = NAME;

