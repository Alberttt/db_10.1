-- создать связь между таблицами многие-ко-многим 

DROP TABLE IF exists tour, review, city, tour_to_city CASCADE;

CREATE TABLE tour
(
name text PRIMARY KEY,
price int
);

CREATE TABLE city
(
title text PRIMARY KEY,
founded text
);

CREATE TABLE review
(
id int PRIMARY KEY,
comment text,
rating int,
tour_name text references tour
);


INSERT INTO tour (name, price)
VALUES ('В гостях у хаски', 1500),
('Хиты в карелии', 2000);


INSERT INTO city (title, founded)
VALUES ('Приозерск', '04.11.2022'),
('Сортавала', '11.11.2022'),
('Валаам', '26.12.2022'),
('Адлер', '10.01.2023');
 
INSERT INTO review (id, comment, rating, tour_name)
VALUES (1, 'Хорошо', 35, 'В гостях у хаски'),
(2, 'Хорошо', 24, 'Хиты в карелии'),
(3, 'Плохо', 80, 'Хиты в карелии'),
(4, 'Плохо', 98, 'В гостях у хаски'),
(5, 'Хорошо', 22, 'Хиты в карелии');


CREATE TABLE tour_to_city
(
tour_name TEXT REFERENCES tour NOT NULL,
city_title TEXT REFERENCES city NOT NULL,
UNIQUE (tour_name, city_title)
);

 
INSERT INTO tour_to_city (tour_name, city_title)
VALUES ('В гостях у хаски', 'Приозерск'),
('Хиты в карелии', 'Сортавала'),
('Хиты в карелии', 'Адлер'),
('В гостях у хаски', 'Валаам'),
('В гостях у хаски', 'Сортавала');
 
SELECT review.rating, tour.name, c.title, c.founded, review.id, tour.price, review.comment

FROM tour
JOIN tour_to_city tc ON tc.tour_name = tour.name
JOIN city c ON c.title = tc.city_title
join review on tour.name = review.tour_name;
 


