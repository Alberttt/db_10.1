 --  создать связь между таблицами один-к-одному
 
DROP TABLE IF EXISTS users, settings, CASCADE;
 
CREATE TABLE users
(
nickname TEXT PRIMARY KEY,
first_name TEXT,
last_name TEXT
);
 
CREATE TABLE settings
(
font_size INT,
color_scheme TEXT,
user_nickname TEXT REFERENCES users UNIQUE
);
 
INSERT INTO users (nickname, first_name, last_name)
VALUES ('katikovka', 'Екатерина', 'Орлова'),
       ('Kexibo', 'Виталий', 'Суранов');
      
INSERT INTO settings (font_size, color_scheme, user_nickname)
VALUES (14, 'Синий', 'katikovka'),
       (14, 'Жёлтый', 'Kexibo');
      
SELECT *
FROM users
         JOIN settings ON nickname = user_nickname;

